package com.michalczop.java.elasticsearchdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.michalczop.java.elasticsearchdemo.domain.Book;
import com.michalczop.java.elasticsearchdemo.domain.ScrollResponse;
import com.michalczop.java.elasticsearchdemo.domain.Search;
import com.michalczop.java.elasticsearchdemo.service.JavaApiBookService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@SpringBootTest
class JavaApiBookServiceTests {

	@Autowired
	JavaApiBookService service;

	@Autowired
	RestHighLevelClient client;

	@Autowired
	private ObjectMapper objectMapper;


	@Test
	void getBookTest() {
		Book book = service.getBookById("1");
		log.info("Found book: {}", book);
		assertNotNull(book);
	}

	@Test
	void searchBooksForAuthorTest() {
		List<Book> books = service.findBooksForAuthor("Stephen King");
		log.info("Found books: {}", books);
		Assertions.assertTrue(books != null && books.size() > 0);
	}

	@Test
	void getBooksForAuthorTest() {
		List<Book> books = service.getBooksForAuthor("Stephen King");
		log.info("Found books: {}", books);
		Assertions.assertTrue(books != null && books.size() > 0);
	}

	@Test
	void getBookForAuthorAndTitleTest() {
		Book book = service.getBookForAuthorAndTitle("Stephen King", "Bastion");
		log.info("Found book: {}", book);
		Assertions.assertTrue(book != null);
	}

	@Test
	void getAllBooksTest() {
		List<Book> books = service.findAll();
		log.info("Found books: {}", books);
		Assertions.assertTrue(books != null && books.size() > 0);
	}

	@Test
	void existsTest() {
		boolean exists = service.exists("1");
		log.info("Exists result: {}", exists);
		Assertions.assertTrue(exists);
	}

	@Test
	void indexBookTest() {
		service.indexBook(new Book("testBook", "test title", "test author", "test genre1", "2020-01-01", 310, Arrays.asList(3.0, 4.5, 5.0, 4.5)));
		Assertions.assertTrue(service.exists("testauthortesttitle"));
	}

	@Test
	void deleteTest() {
		service.indexBook(new Book("testauthortesttitle", "test title", "test author", "test genre", "2020-01-01", 300, Arrays.asList(3.0, 4.5, 5.0, 4.5)));
		boolean existsAfterIndex = service.exists("testauthortesttitle");
		service.deleteBook("testauthortesttitle");
		boolean existsAfterDelete = service.exists("testauthortesttitle");
		Assertions.assertTrue(existsAfterIndex);
		Assertions.assertFalse(existsAfterDelete);
	}

	@Test
	public void findBooksTest() {
		List<Book> books;
		Search search;

		search = Search.builder().author("Stephen King").build();
		books = service.findBooks(search);
		Assertions.assertTrue(books != null && books.size() > 0);

		search = Search.builder().title("Lśnienie").build();
		books = service.findBooks(search);
		Assertions.assertTrue(books != null && books.size() > 0);

		search = Search.builder().genre("Horror").build();
		books = service.findBooks(search);
		Assertions.assertTrue(books != null && books.size() > 0);

		search = Search.builder().author("Stephen King").title("Lśnienie").genre("Horror").build();
		books = service.findBooks(search);
		Assertions.assertTrue(books != null && books.size() > 0);

		search = Search.builder().author("Stephen King").title("Wrong Title").build();
		books = service.findBooks(search);
		Assertions.assertTrue(books != null && books.size() == 0);
	}

	@Test
	public void findBooksInYearsTest() {
		List<Book> books = service.findBooksInYears(new Date(70, 1, 1), new Date(90, 1,1));
		log.info("Found books: {}", books);
		Assertions.assertTrue(books != null && books.size() > 0);
	}

	@Test
	public void getBookCountByAuthorTest() {
		List<? extends Terms.Bucket> buckets = service.getBookCountByAuthor();
		Assertions.assertTrue(buckets != null && buckets.size() > 0);
	}

	@Test
	public void getAvgRatingsForAuthorTest() {
		List<? extends Terms.Bucket> buckets = service.getAvgRatingsForAuthor("Stephen King");
		Assertions.assertTrue(buckets != null && buckets.size() > 0);
	}

	@Test
	public void aTest() {
		List<? extends Terms.Bucket> buckets = service.getMostRecentBookForAuthors();
		Assertions.assertTrue(buckets != null && buckets.size() > 0);
	}

	@Test
	public void findAllByScrollPOC() {

		ScrollResponse resp = service.findAllByScroll();
		log.info("First part of books: {}", resp.getBooks());
		Assertions.assertTrue(resp.getBooks().size() > 0);

		resp = service.findAllByScroll(resp.getScrollId());
		log.info("Second part of books: {}", resp.getBooks());
		Assertions.assertTrue(resp.getBooks().size() > 0);
	}

	@Test
	public void test() {

		String hitsAsString = "L=0,V=0,C=0,D=0,B=1,M=0,SUM=1";

		Boolean res = hitsAsString.split("SUM=")[1] == "0";

		System.out.println();
	}
}
