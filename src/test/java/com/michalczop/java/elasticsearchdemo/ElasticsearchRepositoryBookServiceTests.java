package com.michalczop.java.elasticsearchdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.michalczop.java.elasticsearchdemo.domain.Book;
import com.michalczop.java.elasticsearchdemo.service.ElasticsearchRepositoryBookService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@SpringBootTest
class ElasticsearchRepositoryBookServiceTests {

	@Autowired
	private ElasticsearchRepositoryBookService bookService;

	@Autowired
	RestHighLevelClient client;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void saveBookTest() {
		Book book = new Book("testBook", "test title", "test author", "test genre", "2020-01-01", 300, Arrays.asList(3.0, 4.5, 5.0, 4.5));
		Book resp = bookService.save(book);
		log.info("saveBookTest finished");
	}

	@Test
	void getBookTest() {
		Optional<Book> resp = bookService.findOne("1");
		log.info("getBookTest: Recived book {}.", resp.get());
	}

	@Test
	void deleteBookTest() {
		Book book = new Book("testBook", "test title", "test author", "test genre", "2020-01-01", 300, Arrays.asList(3.0));
		bookService.save(book);
		Optional<Book> resp = bookService.findOne("testBook");
		log.info("getBookTest: Recived book {}", resp.get());
		if(resp.isPresent()) {
			bookService.delete(resp.get());
		} else {
			log.error("Book not found");
		}
		log.info("deleteBookTest finished.");
	}

	@Test
	void getAllBooksTest() {
		Iterable<Book> books = bookService.findAll();
		List<Book> bookList = ((AggregatedPageImpl) books).getContent();
		log.info("getAllBooksTest: Recived books {}.", bookList);

	}
}
