package com.michalczop.java.elasticsearchdemo.service;

import com.michalczop.java.elasticsearchdemo.domain.Book;
import com.michalczop.java.elasticsearchdemo.domain.ScrollResponse;
import com.michalczop.java.elasticsearchdemo.domain.Search;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import java.util.Date;
import java.util.List;

public interface JavaApiBookService {

    boolean exists(String id);

    Book getBookById(String id);

    List<Book> findBooksForAuthor(String author);

    List<Book> getBooksForAuthor(String author);

    Book getBookForAuthorAndTitle(String author, String title);

    List<Book> findAll();

    void indexBook(Book b);

    void deleteBook(String id);

    List<Book> findBooks(Search search);

    List<Book> findBooksInYears(Date from, Date to);

    List<? extends Terms.Bucket> getBookCountByAuthor();

    List<? extends Terms.Bucket> getAvgRatingsForAuthor(String author);

    List<? extends Terms.Bucket> getMostRecentBookForAuthors ();

    ScrollResponse findAllByScroll();

    ScrollResponse findAllByScroll(String scrollId);
}
