package com.michalczop.java.elasticsearchdemo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.michalczop.java.elasticsearchdemo.domain.Book;
import com.michalczop.java.elasticsearchdemo.domain.ScrollResponse;
import com.michalczop.java.elasticsearchdemo.domain.Search;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.TopHits;
import org.elasticsearch.search.aggregations.metrics.TopHitsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JavaApiBookServiceImpl implements JavaApiBookService{

    public final static String BOOKS_INDEX = "books";

    @Autowired
    RestHighLevelClient client;

    @Autowired
    private ObjectMapper objectMapper;

    public boolean exists(String id) {
        GetRequest getRequest = new GetRequest(BOOKS_INDEX, id);
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        getRequest.storedFields("_none_");

        try {
            boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
            return exists;
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return false;
    }

    public Book getBookById(String id) {
        GetRequest getRequest = new GetRequest(BOOKS_INDEX, id);

        Book book = null;

        try {
            GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);

            log.info("Response index: {}, response id: {}", getResponse.getIndex(), getResponse.getId());

            if (getResponse.isExists()) {

                /* Possible ways of GetResponse object processing - for demo purposes */

                long version = getResponse.getVersion();
                String sourceAsString = getResponse.getSourceAsString();
                Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();
                byte[] sourceAsBytes = getResponse.getSourceAsBytes();
                book = objectMapper.readValue(sourceAsString, Book.class);

            } else {
                log.info("Document with id {} doesn't exist.", getRequest.id());
            }
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                log.error("NOT FOUND, {}", e.getMessage());
            } else {
                log.error(e.getMessage());
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return book;
    }

    /* MATCH QUERY - Returns books matching some author attributes (first name or last name) */
    public List<Book> findBooksForAuthor(String author) {

        List<Book> books = new ArrayList<Book>();

        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("author", author));
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return books;
    }

    /* TERM QUERY - Returns books matching exactly author as keyword */
    public List<Book> getBooksForAuthor(String author) {

        List<Book> books = new ArrayList<Book>();

        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.termQuery("author.keyword", author)).sort("released", SortOrder.ASC);
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return books;
    }

    public Book getBookForAuthorAndTitle(String author, String title) {

        List<Book> books = new ArrayList<Book>();

        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        BoolQueryBuilder qb = new BoolQueryBuilder();
        qb.must(QueryBuilders.matchQuery("author.keyword", author));
        qb.must(QueryBuilders.matchQuery("title.keyword", title));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(qb);
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
            System.out.println("a");
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return books.size() > 0 ? books.get(0) : null;
    }

    public List<Book> findAll() {

        List<Book> books = new ArrayList<Book>();

        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return books;
    }

    public void indexBook(Book b) {

        Book book = getBookForAuthorAndTitle(b.getAuthor(), b.getTitle());

        if(book == null) {
            book = new Book();
            book.setId(generateId(b));
            book.setAuthor(b.getAuthor());
            book.setTitle(b.getTitle());
        }

        book.setGenre(b.getGenre());
        book.setPages(b.getPages());
        book.setReleased(b.getReleased());
        book.setRatings(b.getRatings());

        Map<String, Object> jsonMap = objectMapper.convertValue(book, Map.class);

        IndexRequest indexRequest = new IndexRequest(BOOKS_INDEX)
                .id(book.getId()).source(jsonMap);

        try {
            IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void deleteBook(String id) {

        DeleteRequest deleteRequest = new DeleteRequest(BOOKS_INDEX, id);

        try {
            DeleteResponse deleteResponse = client.delete(deleteRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    /* Bool query example */
    public List<Book> findBooks(Search search) {

        List<Book> books = new ArrayList<Book>();

        Map<String, Object> map = search.getSearchMap();

        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        BoolQueryBuilder qb = new BoolQueryBuilder();

        map.keySet().stream().forEach(k -> {
            qb.must(QueryBuilders.matchQuery(k, ((String)map.get(k)).toLowerCase()));
        });

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(qb);
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
            System.out.println("a");
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return books;
    }

    /* Range query example */
    public List<Book> findBooksInYears(Date from, Date to) {

        List<Book> books = new ArrayList<Book>();

        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.rangeQuery("released").gte(from).lte(to));
        searchRequest.source(searchSourceBuilder);

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
            System.out.println("a");
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return books;
    }

    /* Aggregation example */
    public List<? extends Terms.Bucket> getBookCountByAuthor() {
        SearchRequest searchRequest = new SearchRequest(JavaApiBookServiceImpl.BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        TermsAggregationBuilder aggregation = AggregationBuilders.terms("by_author")
                .field("author.keyword");
        searchSourceBuilder.aggregation(aggregation);
        searchRequest.source(searchSourceBuilder);

        List<? extends Terms.Bucket> buckets = null;

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);

            Aggregations aggregations = searchResponse.getAggregations();
            Terms byAuthorAggregation = aggregations.get("by_author");
            buckets = byAuthorAggregation.getBuckets();
            for (Terms.Bucket bt : buckets) {
                log.info("For author {} I found {} books.", bt.getKeyAsString(), bt.getDocCount());
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return buckets;
    }

    /* Avg aggregation example */
    public List<? extends Terms.Bucket> getAvgRatingsForAuthor(String author) {
        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("author.keyword", "Stephen King"));

        TermsAggregationBuilder aggregation = AggregationBuilders.terms("by_Stephen_King")
                .field("title.keyword");
        aggregation.subAggregation(AggregationBuilders.avg("avg_rating")
                .field("ratings"));

        searchSourceBuilder.aggregation(aggregation);

        searchRequest.source(searchSourceBuilder);

        List<? extends Terms.Bucket> buckets = null;

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);

            Aggregations aggregations = searchResponse.getAggregations();
            Terms byAuthorAggregation = aggregations.get("by_Stephen_King");
            buckets = byAuthorAggregation.getBuckets();
            for (Terms.Bucket bt : buckets) {
                Avg average = bt.getAggregations().get("avg_rating");
                double averageRating = average.getValue();
                log.info("For title {} average rating is {}.", bt.getKeyAsString(), averageRating);
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return buckets;
    }

    /* TopHits aggregation example */
    public List<? extends Terms.Bucket> getMostRecentBookForAuthors () {
        SearchRequest searchRequest = new SearchRequest(JavaApiBookServiceImpl.BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        TopHitsAggregationBuilder topHitsAggregationBuilder = new TopHitsAggregationBuilder("TOP_HITS");
        topHitsAggregationBuilder.size(1).sort("released", SortOrder.DESC);

        TermsAggregationBuilder aggregation = AggregationBuilders.terms("by_author")
                .field("author.keyword");
        aggregation.subAggregation(topHitsAggregationBuilder);
        searchSourceBuilder.aggregation(aggregation);
        searchRequest.source(searchSourceBuilder);

        List<? extends Terms.Bucket> buckets = null;

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);

            Aggregations aggregations = searchResponse.getAggregations();
            Terms byAuthorAggregation = aggregations.get("by_author");
            buckets = byAuthorAggregation.getBuckets();
            for (Terms.Bucket bt : buckets) {
                TopHits th = bt.getAggregations().get("TOP_HITS");
                SearchHit sh = th.getHits().getHits()[0];
                log.info("For author {}, the most recent book is {}, {}", bt.getKeyAsString(), sh.getSourceAsMap().get("title"), sh.getSourceAsMap().get("released"));
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return buckets;
    }

    public ScrollResponse findAllByScroll() {

        List<Book> books = new ArrayList<Book>();
        String scrollId;

        SearchRequest searchRequest = new SearchRequest(BOOKS_INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(3);
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);
        searchRequest.scroll(TimeValue.timeValueMinutes(10L));

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            scrollId = searchResponse.getScrollId();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
            return new ScrollResponse(books, scrollId);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public ScrollResponse findAllByScroll(String scrollId) {

        List<Book> books = new ArrayList<Book>();

        try {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            SearchResponse searchScrollResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
            scrollId = searchScrollResponse.getScrollId();
            SearchHits hits = searchScrollResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            books = Arrays.stream(searchHits).map(h -> {
                return objectMapper.convertValue(h.getSourceAsMap(), Book.class);
            }).collect(Collectors.toList());
            return new ScrollResponse(books, scrollId);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /* For this demo example, Book Id consists of author and title */
    private String generateId(Book book) {
        return book.getAuthor().replaceAll("\\s+","") + book.getTitle().replaceAll("\\s+","");
    }
}
