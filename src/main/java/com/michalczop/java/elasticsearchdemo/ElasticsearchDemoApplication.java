package com.michalczop.java.elasticsearchdemo;

import com.michalczop.java.elasticsearchdemo.service.ElasticsearchRepositoryBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ElasticsearchDemoApplication {

	@Autowired
	private ElasticsearchRepositoryBookService bookService;

	public static void main(String[] args) {
		SpringApplication.run(ElasticsearchDemoApplication.class, args);
	}
}
