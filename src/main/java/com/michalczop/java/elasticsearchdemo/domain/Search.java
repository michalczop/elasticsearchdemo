package com.michalczop.java.elasticsearchdemo.domain;

import lombok.Builder;

import java.util.HashMap;
import java.util.Map;

@Builder
public class Search {
    private String title;
    private String author;
    private String genre;

    public Map<String, Object> getSearchMap() {

        Map<String, Object> searchMap = new HashMap<String, Object>();

        if(title != null) {
            searchMap.put("title", title);
        }
        if(author != null) {
            searchMap.put("author", author);
        }
        if(genre != null) {
            searchMap.put("genre", genre);
        }

        return searchMap;
    }
}
