package com.michalczop.java.elasticsearchdemo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "books")
public class Book {

    @Id
    private String id;

    private String title;
    private String author;
    private String genre;
    private String released;
    private int pages;
    private List<Double> ratings;

}
