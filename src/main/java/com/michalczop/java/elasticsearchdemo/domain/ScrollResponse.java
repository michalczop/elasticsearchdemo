package com.michalczop.java.elasticsearchdemo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScrollResponse {

    private List<Book> books;
    private String scrollId;

}
