package com.michalczop.java.elasticsearchdemo.controller;

import com.michalczop.java.elasticsearchdemo.domain.Book;
import com.michalczop.java.elasticsearchdemo.domain.ScrollResponse;
import com.michalczop.java.elasticsearchdemo.domain.Search;
import com.michalczop.java.elasticsearchdemo.service.JavaApiBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("java-api")
public class JavaApiBookController {

    @Autowired
    private JavaApiBookService bookService;

    @GetMapping("/book")
    public List<Book> getAllBooks() {
        List<Book> books = bookService.findAll();
        return books;
    }

    @GetMapping("/book/find")
    public List<Book> findBook(@RequestParam(required = false) String author, @RequestParam(required = false) String title, @RequestParam(required = false) String genre) {
        Search search = Search.builder().author(author).title(title).genre(genre).build();
        return bookService.findBooks(search);
    }

    @GetMapping("/book/scroll")
    public ScrollResponse findAllByScroll() {
        return bookService.findAllByScroll();
    }

    @GetMapping("/book/scroll/{scrollId}")
    public ScrollResponse findAllByScroll(@PathVariable String scrollId) {
        return bookService.findAllByScroll(scrollId);
    }

    @PostMapping("/book")
    public void indexBook(@RequestBody Book book) {
        bookService.indexBook(book);
    }

    @DeleteMapping("/book/{id}")
    public void deleteBook(@PathVariable String id) {
        bookService.deleteBook(id);
    }
}
