package com.michalczop.java.elasticsearchdemo.controller;

import com.michalczop.java.elasticsearchdemo.domain.Book;
import com.michalczop.java.elasticsearchdemo.service.ElasticsearchRepositoryBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("elasticsearch-repository")
public class ElastisearchRepositoryBookController {

    @Autowired
    private ElasticsearchRepositoryBookService bookService;

    @GetMapping("/book")
    public List getAllBooks() {
        Iterable<Book> books = bookService.findAll();
        return ((AggregatedPageImpl) books).getContent();
    }

    @GetMapping("/book/{id}")
    public Book getAllBooks(@PathVariable String id) {
        return bookService.findOne(id).get();
    }

    @PostMapping("/book/add")
    public Book addBook(@RequestBody Book book) {
        return  bookService.save(book);
    }
}
