# Elasticsearch Demo

Projekt opiera się na instancji Elasticsearch 7.8. działającej w kontenerze Docker.

## Uruchomienie projektu

##### 1. Pobranie i uruchomienie obrazu Elasticsearch
    docker pull docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.8.0

##### 2. Utworzenie indeksu books wraz z mappingiem (request zawarty w załączonej kolekcji Postman).

##### 3. Import przykładowych danych z pliku books-data.json znajdującego się w repozytorium przy pomocy komendy:
    
    curl -H "Content-Type: application/x-ndjson" -XPOST http://localhost:9200/books/_bulk?pretty --data-binary "@books-data.json"

##### 4. Uruchmienie aplikacji spring-boot
